import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/layout';
import Head from '../components/head';

const IndexPage = () => {
  return (
    <Layout>
      <Head title="Home" />
      <h1>Hey,</h1>
      <h2>I'm Bidhan.</h2>
      <p>
        Check out my <Link to="/blog">blog</Link>.
      </p>
    </Layout>
  );
};

export default IndexPage;
