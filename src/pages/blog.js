import React from 'react';
import { Link, graphql, useStaticQuery } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import Layout from '../components/layout';
import Head from '../components/head';
import * as blogStyles from './blog.module.scss';

const BlogPage = () => {
  // const data = useStaticQuery(graphql`
  //   query {
  //     allContentfulBlogPost(sort: { publishedDate: DESC }) {
  //       edges {
  //         node {
  //           title
  //           slug
  //           publishedDate(formatString: "MMMM Do, YYYY")
  //         }
  //       }
  //     }
  //   }
  // `);

  const data = useStaticQuery(graphql`
  query {
    contentfulAsset(title: { eq: "Cartman Dance"}) {
      title
      file {
        url
      }
    }
  }
  `);

  return (
    <Layout>
      <Head title="Blog" />
      <ol className={blogStyles.posts}>
        <h3>Coming Soon</h3>
        <img className={blogStyles.tempImage} src={data.contentfulAsset.file.url} alt={data.contentfulAsset.title} />
        {/* {data.allContentfulBlogPost.edges.map(edge => {
          return (
            <li className={blogStyles.post}>
              <Link to={`/blog/${edge.node.slug}`}>
                <h2>{edge.node.title}</h2>
                <p>{edge.node.publishedDate}</p>
              </Link>
            </li>
          );
        })} */}
      </ol>
    </Layout>
  );
};

export default BlogPage;
