---
title: 'Gatsby is cool'
date: '2019-11-15'
---

### I just started to learn Gatsby...

![Gatsby Image](./gatsby.png)

...and I am enjoying it so far. It seems great and I can't wait to create an awesome personal website using Gatsby.

### Important topics

1. Gatsby
2. GraphQL
3. React
